from django.contrib import admin
from . import models

admin.site.register(models.News)
admin.site.register(models.Warning)
admin.site.register(models.User)
admin.site.register(models.Review)
admin.site.register(models.CourseLCMP)
admin.site.register(models.StepOne)
admin.site.register(models.StepTwo)
admin.site.register(models.StepThree)
admin.site.register(models.StepFour)
admin.site.register(models.Diary)
