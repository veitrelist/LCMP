import django.forms as forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordResetForm, SetPasswordForm, AuthenticationForm, UsernameField


class UserCreationForm(forms.ModelForm):

    username = forms.CharField(widget=forms.EmailInput(attrs={'class': "input"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': "input"}))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].label = 'Логин'
        self.fields['password'].label = 'Пароль'

    def clean(self):
        password = self.cleaned_data['password']
        username = self.cleaned_data['username']

        return self.cleaned_data

    class Meta:
        model = User
        fields = ['username', 'password']


class LoginForm(forms.ModelForm):

    username = forms.CharField(widget=forms.TextInput(attrs={'class': "input"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': "input"}))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].label = 'Логин'
        self.fields['password'].label = 'Пароль'

    def clean(self):
        password = self.cleaned_data['password']
        username = self.cleaned_data['username']

        return self.cleaned_data

    class Meta:
        model = User
        fields = ['username', 'password']


class ResetPasswordForm(PasswordResetForm):
    email = forms.EmailField(
        label="Email",
        max_length=254,
        widget=forms.EmailInput(attrs={'autocomplete': 'email', 'class': 'input'})
    )


class ResetPasswordConfirmForm(SetPasswordForm):
    new_password1 = forms.CharField(
        label="New password",
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password', 'class': 'input'}),
        strip=False,
    )
    new_password2 = forms.CharField(
        label="New password confirmation",
        strip=False,
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password', 'class': 'input'}),
    )

