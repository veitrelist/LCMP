from django.http import HttpResponse
from django.shortcuts import render, redirect
from .forms import UserCreationForm, LoginForm
from django.views.generic import View
from django.contrib.auth import authenticate, login
from .models import User as Account
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.conf import settings
from django.utils.encoding import force_text, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_decode
from .utils import token_generator, EmailSendThread
from .services import email_body_for_activation


class RegistrationView(View):

    def get(self, request, *args, **kwargs):
        form = UserCreationForm(request.POST or None)
        context = {'form': form}
        return render(request, 'registration.html', context)

    def post(self, request, *args, **kwargs):
        form = UserCreationForm(request.POST or None)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            if User.objects.filter(username=username).exists():
                return render(request, 'registration.html', {'form': form,
                                                             'errors': {'username': f'"{username}" уже существует'}})
            if len(password) < 8:
                return render(request, 'registration.html',
                              {'form': form, 'errors': {'password': 'Пароль должен быть не менее 8 символов'}})

            new_user = form.save(commit=False)
            new_user.username = username
            new_user.email = username
            new_user.save()
            new_user.set_password(password)
            new_user.is_active = False
            new_user.save()
            Account.objects.create(user_auth=User.objects.get(username=new_user.username))

            email_header = 'Activation message'

            email_message = EmailMessage(
                email_header,
                email_body_for_activation(request, new_user),
                settings.EMAIL_HOST_USER,
                [new_user.username],
            )
            EmailSendThread(email_message).start()

            return render(request, 'RestorationMessage.html', {})
        context = {'form': form}
        return render(request, 'registration.html', context)


class VerificationView(View):

    def get(self, request, uidb64, token):
        try:
            id = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=id)

            if not token_generator.check_token(user, token):
                return redirect('login')
            if user.is_active:
                return redirect('login')
            user.is_active = True
            user.save()
            return render(request, 'ActivationMsg.html', {})
        except DjangoUnicodeDecodeError:
            pass


class LoginView(View):

    def get(self, request, *args, **kwargs):
        form = LoginForm(request.POST or None)
        context = {'form': form}
        return render(request, 'login.html', context)

    def post(self, request, *args, **kwargs):
        form = LoginForm(request.POST or None)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = User.objects.filter(username=username).exists()
            if user:
                user = User.objects.get(username=username)
                if user.check_password(password):
                    user = authenticate(username=username, password=password)
                    login(request, user)
                    return redirect('about')
                return render(request, 'login.html', {'form': form, 'errors': {'password': 'Неверный пароль'}})
            return render(request, 'login.html', {'form': form, 'errors': {'username': 'Пользователь не найден'}})
        return render(request, 'login.html', {'from': form})


class ReviewsView(View):

    def get(self, request, *args, **kwargs):
        context = {}
        return render(request, 'Reviews.html', context)


class AboutView(View):

    def get(self, request, *args, **kwargs):
        context = {}
        return render(request, 'o.html', context)


class MainView(View):

    def get(self, request, *args, **kwargs):
        context = {}
        return render(request, 'main.html', context)

