from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.contrib.sites.shortcuts import get_current_site
from .utils import token_generator


def _get_activate_url_for_user(request, user):
    domain = get_current_site(request).domain
    link = reverse('activate',
                   kwargs={'uidb64': urlsafe_base64_encode(force_bytes(user.pk)),
                           'token': token_generator.make_token(user)})

    return str('http://' + domain + link)


def email_body_for_activation(request, user):
    activation_url = _get_activate_url_for_user(request, user)
    email_body = f'What\'s up! Here it is your link to verify your account {user.username}\n ' \
                 f'{activation_url}\n Thanks for using our service!'
    return email_body
