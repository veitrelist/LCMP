from django.urls import path
from .views import *
from django.contrib.auth import views as auth_views
from .forms import ResetPasswordForm, ResetPasswordConfirmForm

urlpatterns = [
    path('signup/', RegistrationView.as_view(), name='signup'),
    path('activate/<uidb64>/<token>', VerificationView.as_view(), name='activate'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='/'), name='logout'),

    path('reset_password/',
         auth_views.PasswordResetView.as_view(template_name='Restoration.html',
                                              form_class=ResetPasswordForm),
         name='reset_password'),

    path('reset_password_sent/', auth_views.PasswordResetDoneView.as_view(
                                        template_name='RestorationMessage.html'
    ), name='password_reset_done'),

    path('reset/<uidb64>/<token>',
         auth_views.PasswordResetConfirmView.as_view(template_name='ChangePassword.html',
                                                     form_class=ResetPasswordConfirmForm),
         name='password_reset_confirm'),

    path('reset_password_complete/', auth_views.PasswordResetCompleteView.as_view(
        template_name='RestorationCompleteMessage.html'), name='password_reset_complete'),

    path('reviews/', ReviewsView.as_view(), name='reviews'),
    path('about/', AboutView.as_view(), name='about'),
    path('', MainView.as_view(), name='main'),

]
