from django.contrib.auth.tokens import PasswordResetTokenGenerator
from six import text_type
from threading import Thread


class AppTokenGenerator(PasswordResetTokenGenerator):

    def _make_hash_value(self, user, timestamp):
        return text_type(user.is_active) + text_type(user.pk) + text_type(timestamp)


token_generator = AppTokenGenerator()


class EmailSendThread(Thread):

    def __init__(self, email_message):
        self.email_message = email_message
        Thread.__init__(self)

    def run(self):
        self.email_message.send()
