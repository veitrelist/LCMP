from django.db import models
from django.contrib.auth.models import User


class News(models.Model):
    text = models.TextField(verbose_name='Текст')

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'


class Warning(models.Model):
    text = models.TextField(verbose_name='Текст')

    class Meta:
        verbose_name = 'Предупреждение'
        verbose_name_plural = 'Предупреждения'


class User(models.Model):
    user_auth = models.OneToOneField(User, on_delete=models.CASCADE, null=True, verbose_name='Аутентификатор')
    full_name = models.CharField(max_length=255, verbose_name='ФИО')
    subscription = models.BooleanField(null=True, verbose_name='Подписка')
    # null=True убрать можно, просто я связь проверял
    review = models.IntegerField(null=True, verbose_name='Отзыв')
    # null=True убрать можно, просто я связь проверял
    course_LCMP = models.IntegerField(null=True, verbose_name='Курс ПГМД')
    # null=True убрать можно, просто я связь проверял

    def __str__(self):
        return str(self.user_auth)

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


class Review(models.Model):
    text = models.TextField(verbose_name='Текст')
    rating = models.IntegerField(verbose_name='Рейтинг')

    class Meta:
        verbose_name = 'Рейтинг'
        verbose_name_plural = 'Рейтинги'


class CourseLCMP(models.Model):
    step_one = models.ForeignKey('StepOne', on_delete=models.CASCADE, verbose_name='Первый Этап')
    step_two = models.ForeignKey('StepTwo', on_delete=models.CASCADE, verbose_name='Второй Этап')
    step_three = models.ForeignKey('StepThree', on_delete=models.CASCADE, verbose_name='Третий Этап')
    step_four = models.ForeignKey('StepFour', on_delete=models.CASCADE, verbose_name='Четвертый Этап')
    health = models.IntegerField(verbose_name='Здоровье')
    warning = models.IntegerField(verbose_name='Предупреждение')

    class Meta:
        verbose_name = 'Курс ПГМД'
        verbose_name_plural = 'Курсы ПГМД'


class StepOne(models.Model):
    diary = models.ForeignKey('Diary', on_delete=models.CASCADE, verbose_name='Дневник')
    result = models.BooleanField(verbose_name='Результат')

    class Meta:
        verbose_name = 'Первый этап'
        verbose_name_plural = 'Первые этапы'


class StepTwo(models.Model):
    diary = models.ForeignKey('Diary', on_delete=models.CASCADE, verbose_name='Дневник')
    result = models.BooleanField(verbose_name='Результат')

    class Meta:
        verbose_name = 'Второй этап'
        verbose_name_plural = 'Вторые этапы'


class StepThree(models.Model):
    diary = models.ForeignKey('Diary', on_delete=models.CASCADE, verbose_name='Дневник')
    result = models.BooleanField(verbose_name='Результат')

    class Meta:
        verbose_name = 'Третий этап'
        verbose_name_plural = 'Третьи этапы'


class StepFour(models.Model):
    diary = models.ForeignKey('Diary', on_delete=models.CASCADE, verbose_name='Дневник')
    result = models.BooleanField(verbose_name='Результат')

    class Meta:
        verbose_name = 'Четвертый этап'
        verbose_name_plural = 'Четвертые этапы'


class Diary(models.Model):
    text = models.TextField(verbose_name='Текст')
    photo = models.TextField(verbose_name='Фото')
    rating = models.IntegerField(verbose_name='Рейтинг')

    class Meta:
        verbose_name = 'Дневник'
        verbose_name_plural = 'Дневники'
